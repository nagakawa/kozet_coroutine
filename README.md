## kozet_coroutine

A nice coroutine library with x64 assembly voodoo.

### Compatibility

Currently, the library assumes that you have an x86-64 processor and your
compiler uses either the System V or the Microsoft calling conventions.
It's probably straightforward to extend this to other architectures.

C11 is required to build the library itself. The header can be included
under C99 or later, or under C++17 or later.

### Building

`make` will build both a static library (`build/libkozet_coroutine.a`) and a
test program (`build/test`). The header file is at
`include/kozet_coroutine/kcr.h`.

### Usage

See the [docs](https://nagakawa.gitlab.io/kozet_coroutine/). Alternatively,
consult `test/main.c` for an example.

### Licence

   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
