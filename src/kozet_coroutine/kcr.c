/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef __linux__
#  define _GNU_SOURCE
#endif

#include "kozet_coroutine/kcr.h"

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include <stdbool.h>

#if !defined(KCR_VERBOSE) || !defined(KCR_AUDIT)
#  error "Please define both KCR_VERBOSE and KCR_AUDIT."
#endif

// Macro to optionally expand to _Thread_local

#ifdef KCR_MULTITHREADED
#  define THREADLOCAL _Thread_local
#else
#  define THREADLOCAL
#endif

// Macro to mark function as maybe unused to suppress warnings
#ifdef __GNUC__
#  define KCR_MAYBE_UNUSED __attribute__((unused))
#else
#  define KCR_MAYBE_UNUSED
#endif

// Define macros to allocate and free aligned memory

#define DEFAULT_STACK_SIZE 65536
#define STACK_ALIGNMENT 16

#if __STDC_VERSION__ >= 201112L // C11
#  define STACKALLOC(align, size) aligned_alloc(align, size)
#  define STACKFREE(ptr) free(ptr)
#elif _POSIX_C_SOURCE >= 200112L // POSIX
void* STACKALLOC(size_t align, size_t size) {
  void* p;
  int stat = posix_memalign(&p, align, size);
  if (stat != 0) return 0;
  return p;
}
#  define STACKFREE(ptr) free(ptr)
#elif _WIN32
#  include <malloc.h>
#  define STACKALLOC(align, size) _aligned_malloc(size, align)
#  define STACKFREE(ptr) _aligned_free(ptr)
#else
#  error "I don't know how to allocate aligned memory!"
#endif

#define INITIAL_QUEUE_SIZE 8

// Global variables to get the current manager & entry inside a coroutine
// so we don't have to lug them around.
// If you want to have this library be thread-safe but don't want to use
// _Thread_local, then you can define the corresponding methods below.

static THREADLOCAL KCRManager* currentKCRManager = 0;
static THREADLOCAL KCREntryHandle currentKCREntry = -1;

KCRManager* kcrManagerGetCurrent(void) { return currentKCRManager; }
KCREntryHandle kcrEntryGetCurrent(void) { return currentKCREntry; }
static void kcrManagerSetCurrent(KCRManager* m) { currentKCRManager = m; }
static void kcrEntrySetCurrent(KCREntryHandle e) { currentKCREntry = e; }

// Internal log macro
#if KCR_VERBOSE
#  define LOGF(...) fprintf(stderr, __VA_ARGS__)
#else
#  define LOGF(...)
#endif

/*

Structure of KCREntry as of the new version:

[ stack - - - - - - - - - - - - - - ] [ metadata ] [ user data - - - - - ]

Here, the stack base is equal to the metadata field.

And soon, you shall see the reason why this library requires C99. Yes,
//-comments, declare-anywhere and bool are nice, but...

*/

typedef struct KCREntry {
  void (*callback)(void*); // The function to actually execute
  void (*tearDown)(void*); // Destructor to be called when it exits
  size_t stackSize; // Stack size
  void* stackBase;
  void* stackPointer; // Stack pointer
  KCREntryHandle next; // Handle to next coroutine that should be executed
  KCREntryHandle prev; // Handle to previous coroutine
  size_t size; // Size of free region (in KCREntry's) if free; otherwise 0
  size_t sizePrev; // Size of previous free region before this one
  size_t udSize; // Size of user data area -- munmap wants us to pass the
                 // size of the area to free explicitly
  bool destroying;
} KCREntry;

struct KCRManager {
  KCREntry* queue;
  KCREntryHandle firstOccupied;
  KCREntryHandle firstFree;
  size_t size;
  void* outsideStackPointer;
  size_t defaultStackSize;
  bool advanceOnExit;
};

// Utilities for working with entry handles

static KCREntry* kcrEntryDeref(KCRManager* manager, KCREntryHandle h) {
  return manager->queue + h;
}

const KCREntry* kcrEntryDerefC(const KCRManager* manager, KCREntryHandle h) {
  return manager->queue + h;
}

KCR_MAYBE_UNUSED static bool
inBoundsH(const KCRManager* manager, const KCREntryHandle entry) {
  return entry != KCR_NO_ENTRY && entry < manager->size;
}

static bool inBounds(const KCRManager* manager, const KCREntry* entry) {
  return entry < manager->queue + manager->size;
}

static KCREntry* getNext(KCREntry* e, KCRManager* m) {
  return kcrEntryDeref(m, e->next);
}

static KCREntry* getPrev(KCREntry* e, KCRManager* m) {
  return kcrEntryDeref(m, e->prev);
}

KCR_MAYBE_UNUSED static const KCREntry*
getNextC(const KCREntry* e, const KCRManager* m) {
  return kcrEntryDerefC(m, e->next);
}

KCR_MAYBE_UNUSED static const KCREntry*
getPrevC(const KCREntry* e, const KCRManager* m) {
  return kcrEntryDerefC(m, e->prev);
}

// It's a magic function
bool contextSwitch(void** oldStackPointer, void* newStackPointer);

static void
kcrManagerFreeEntryEx(KCRManager* manager, KCREntryHandle node, bool nodeOnly);

// Functions for allocating stacks
#if _POSIX_C_SOURCE >= 200112L && !defined(NDEBUG)
#  include <sys/mman.h>
#  include <unistd.h>
// Debug versions of functions that allocate with guard pages.
// It might be possible to specify MAP_GROWSDOWN on platforms that
// support it, but apparently doing so causes some problems.
static void* allocateStack(size_t stackSize, size_t additional) {
  assert(stackSize % STACK_ALIGNMENT == 0);
  size_t pageSize = sysconf(_SC_PAGESIZE);
  size_t totalSize = stackSize + additional + pageSize;
  totalSize = (totalSize + pageSize - 1) / pageSize * pageSize;
  void* mem = mmap(
      0, totalSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (mem == MAP_FAILED) {
    perror("allocateStack");
    return 0;
  }
  assert(((intptr_t) mem) % STACK_ALIGNMENT == 0);
  // Add a guard page that cannot be accessed
  int stat = mprotect(mem, pageSize, PROT_NONE);
  if (stat < 0) {
    munmap(mem, totalSize);
    return 0;
  }
  return (void*) ((char*) mem + stackSize + pageSize);
}
static void freeStack(void* stack, size_t stackSize, size_t additional) {
  assert(stackSize % STACK_ALIGNMENT == 0);
  size_t pageSize = sysconf(_SC_PAGESIZE);
  munmap(
      (char*) stack - stackSize - pageSize, stackSize + additional + pageSize);
}
#else
static void* allocateStack(size_t stackSize, size_t additional) {
  assert(stackSize % STACK_ALIGNMENT == 0);
  void* mem = STACKALLOC(STACK_ALIGNMENT, stackSize + additional);
  if (mem == 0) return 0;
  assert(((intptr_t) mem) % STACK_ALIGNMENT == 0);
  return (void*) ((char*) mem + stackSize);
}
static void freeStack(void* stack, size_t stackSize, size_t additional) {
  (void) additional;
  assert(stackSize % STACK_ALIGNMENT == 0);
  STACKFREE((char*) stack - stackSize);
}
#endif

void kcrReturnFromCoroutine(void);
void kcrTeardownNull(void* userdata) {
  // Nothing
  (void) userdata;
}

void* kcrSetUpStack(void* base, void (*returnAddress)(void*), void* userdata);

// Things related to the scratch stack used when returning from a coroutine
extern void* scratchStack;

size_t kcrDefaultStackSize(void) { return DEFAULT_STACK_SIZE; }

KCRManager* kcrManagerCreate(void) {
  KCRManager* manager = (KCRManager*) malloc(sizeof(*manager));
  if (manager == 0) return 0;
  manager->queue = (KCREntry*) malloc(INITIAL_QUEUE_SIZE * sizeof(KCREntry));
  manager->firstOccupied = KCR_NO_ENTRY;
  manager->size = INITIAL_QUEUE_SIZE;
  manager->defaultStackSize = kcrDefaultStackSize();
  manager->firstFree = 0;
  manager->queue[0].size = INITIAL_QUEUE_SIZE;
  manager->queue[0].sizePrev = 0;
  // Point this node to itself
  manager->queue[0].next = 0;
  manager->queue[0].prev = 0;
  manager->queue[0].stackBase = 0;
  for (size_t i = 1; i < INITIAL_QUEUE_SIZE; ++i) {
    manager->queue[i].size = 0; // Prevent erroneous coälescing
    manager->queue[i].sizePrev = 0;
    manager->queue[i].stackBase = 0;
  }
  manager->advanceOnExit = true;
  return manager;
}

/*
static void debugPrintOccupiedList(KCRManager* man) {
  printf("Occupied:\n");
  KCREntry* e = man->firstOccupied;
  if (e == 0) return;
  KCREntry* f = e;
  do {
    printf("Node at %p (size = %zu, prevSize = %zu)\n", (void*) e, e->size,
e->sizePrev); e = e->next; } while (e != f);
}

static void debugPrintFreeList(KCRManager* man) {
  printf("Free:\n");
  KCREntry* e = man->firstFree;
  if (e == 0) return;
  KCREntry* f = e;
  do {
    printf("Node at %p (size = %zu, prevSize = %zu)\n", (void*) e, e->size,
e->sizePrev); e = e->next; } while (e != f);
}
*/

static void
kcrEntryDestroy(KCRManager* man, KCREntryHandle entry, bool shouldUnwind) {
  KCREntry* e = kcrEntryDeref(man, entry);
  if (shouldUnwind) {
    // Send last-breath return
    e->destroying = true;
    kcrManagerEnter(man, entry);
  }
  e->tearDown(e->stackBase);
}

static void freeStacksOfOccupiedEntries(KCRManager* man) {
  KCREntryHandle e = man->firstOccupied;
  if (e == KCR_NO_ENTRY) return;
  KCREntryHandle f = e;
  do {
    KCREntry* ep = kcrEntryDeref(man, e);
    KCREntryHandle next = ep->next;
    kcrEntryDestroy(man, e, true);
    // Now free the stack for real
    freeStack(ep->stackBase, ep->stackSize, ep->udSize);
    ep->stackBase = 0;
    e = next;
  } while (e != f);
}

void kcrManagerDestroy(KCRManager* manager) {
  LOGF("kcrManagerDestroy called\n");
  freeStacksOfOccupiedEntries(manager);
  free(manager->queue);
  free(manager);
}

#if KCR_AUDIT
// Perform an audit of the KCRManager structure to catch inconsistencies early.
static void
kcrAssertInBounds(const KCRManager* manager, const KCREntryHandle entry) {
  assert(entry < manager->size);
}
static void
kcrCheckConsistencyOnEntry(const KCRManager* manager, KCREntryHandle entry) {
  const KCREntry* e = kcrEntryDerefC(manager, entry);
  kcrAssertInBounds(manager, e->next);
  kcrAssertInBounds(manager, e->prev);
  assert(getNextC(e, manager)->prev == entry);
  assert(getPrevC(e, manager)->next == entry);
}
static void kcrCheckConsistencyOnFreeEntry(
    const KCRManager* manager, const KCREntryHandle entry) {
  const KCREntry* e = kcrEntryDerefC(manager, entry);
  assert(e->size != 0);
  kcrAssertInBounds(manager, entry - e->sizePrev);
  // There should not be two free blocks in a row
  if (e + e->size < manager->queue + manager->size)
    assert((e + e->size)->size == 0);
  if (e->sizePrev != 0) assert((e - e->sizePrev)->size == 0);
}
static void kcrCheckConsistency(const KCRManager* manager) {
  if (manager->firstOccupied != KCR_NO_ENTRY) {
    kcrAssertInBounds(manager, manager->firstOccupied);
    KCREntryHandle eh = manager->firstOccupied;
    do {
      assert(eh != KCR_NO_ENTRY);
      kcrCheckConsistencyOnEntry(manager, eh);
      const KCREntry* e = kcrEntryDerefC(manager, eh);
      assert(
          (const char*) e->stackPointer >=
          ((const char*) e->stackBase) - e->stackSize);
      assert(e->size == 0);
      eh = e->next;
    } while (eh != manager->firstOccupied);
  }
  if (manager->firstFree != KCR_NO_ENTRY) {
    kcrAssertInBounds(manager, manager->firstFree);
    KCREntryHandle eh = manager->firstFree;
    do {
      assert(eh != KCR_NO_ENTRY);
      kcrCheckConsistencyOnEntry(manager, eh);
      kcrCheckConsistencyOnFreeEntry(manager, eh);
      eh = kcrEntryDerefC(manager, eh)->next;
    } while (eh != manager->firstFree);
  }
}
static void traceRegions(const KCRManager* manager) {
  // Give a visual indication of free and occupied regions
  size_t i = 0;
  while (i < manager->size) {
    const KCREntry* e = manager->queue + i;
    if (e->size == 0) {
      fprintf(stderr, "o");
      ++i;
    } else {
      for (size_t j = 0; j < e->size; ++j) { fprintf(stderr, "f"); }
      i += e->size;
    }
  }
  fprintf(stderr, "\n");
}
#else
static void kcrCheckConsistency(const KCRManager* manager) { (void) manager; }
static void traceRegions(const KCRManager* manager) { (void) manager; }
#endif

static KCREntryHandle kcrManagerGetFreeEntry(KCRManager* manager) {
  LOGF("get free entry\n");
  kcrCheckConsistency(manager);
  KCREntryHandle eh = manager->firstFree;
  if (eh == KCR_NO_ENTRY) {
    LOGF("expand\n");
    // No free space left; double capacity
    manager->queue = (KCREntry*) realloc(
        manager->queue, 2 * manager->size * sizeof(KCREntry));
    for (size_t i = manager->size; i < 2 * manager->size; ++i) {
      manager->queue[i].size = 0; // Prevent erroneous coälescing
      manager->queue[i].sizePrev = 0;
      manager->queue[i].stackBase = 0;
    }
    // Relocation is a no-op now, as we no longer store pointers to entries.
    // Create a new free block
    manager->firstFree = manager->size;
    KCREntry* newFree = manager->queue + manager->size;
    newFree->size = manager->size;
    newFree->sizePrev = 1; // since all other entries are taken
    newFree->next = manager->firstFree;
    newFree->prev = manager->firstFree;
    // Update size
    manager->size *= 2;
    eh = manager->firstFree;
  }
  kcrCheckConsistency(manager);
  assert(inBoundsH(manager, eh));
  KCREntry* e = kcrEntryDeref(manager, eh);
  if (e->size == 1) {
    LOGF("remove from free list\n");
    // Remove this block altogether
    if (eh == e->next) {
      // Only free block remaining
      manager->firstFree = KCR_NO_ENTRY;
    } else {
      getNext(e, manager)->prev = e->prev;
      getPrev(e, manager)->next = e->next;
      if (eh == manager->firstFree) manager->firstFree = e->next;
    }
  } else {
    LOGF("slice from free block (size = %zu)\n", e->size);
    // Shave off the first entry
    KCREntryHandle fh = eh + 1;
    KCREntry* f = e + 1;
    assert(inBounds(manager, f));
    f->size = e->size - 1;
    f->next = eh == e->next ? fh : e->next;
    f->prev = eh == e->prev ? fh : e->prev;
    getNext(f, manager)->prev = fh;
    getPrev(f, manager)->next = fh;
    f->sizePrev = 1;
    // Notify the right block that I've shrunk
    KCREntry* succ = e + e->size;
    if (inBounds(manager, succ)) { succ->sizePrev = e->size - 1; }
    if (eh == manager->firstFree) manager->firstFree = fh;
  }
  e->size = 0;
  e->destroying = false;
  traceRegions(manager);
  kcrCheckConsistency(manager);
  return eh;
}

static KCREntryHandle kcrManagerAddEntry(
    KCRManager* manager, KCREntryHandle node, size_t stackSize, size_t udSize) {
  LOGF("get free entry\n");
  if (node == KCR_NO_ENTRY) node = manager->firstOccupied;
  kcrCheckConsistency(manager);
  stackSize = (stackSize + STACK_ALIGNMENT - 1) & ~(STACK_ALIGNMENT - 1);
  KCREntryHandle newNodeHandle = kcrManagerGetFreeEntry(manager);
  if (newNodeHandle == KCR_NO_ENTRY) { return 0; }
  KCREntry* newNode = kcrEntryDeref(manager, newNodeHandle);
  newNode->destroying = false;
  kcrCheckConsistency(manager);
  if (manager->firstOccupied == KCR_NO_ENTRY) {
    // List is empty; make the node point to itself
    newNode->next = newNodeHandle;
    newNode->prev = newNodeHandle;
    manager->firstOccupied = newNodeHandle;
  } else {
    newNode->next = node;
    newNode->prev = kcrEntryDeref(manager, node)->prev;
    getNext(newNode, manager)->prev = newNodeHandle;
    getPrev(newNode, manager)->next = newNodeHandle;
  }
  // We can potentially skip reallocating the stack
  if (newNode->stackBase == 0 || newNode->stackSize < stackSize ||
      newNode->udSize < udSize) {
    if (newNode->stackBase != 0) {
      freeStack(newNode->stackBase, newNode->stackSize, newNode->udSize);
    }
    void* stackBase = allocateStack(stackSize, udSize);
    if (stackBase == 0) {
      kcrManagerFreeEntryEx(manager, newNodeHandle, true);
      return 0;
    }
    newNode->stackBase = stackBase;
  }
  newNode->stackSize = stackSize;
  newNode->udSize = udSize;
  return newNodeHandle;
}

static void
kcrManagerFreeEntryEx(KCRManager* manager, KCREntryHandle node, bool nodeOnly) {
  kcrCheckConsistency(manager);
  // Remove from occupied list
  KCREntry* n = kcrEntryDeref(manager, node);
  if (node == n->next) {
    // Last node in list; delete altogether
    manager->firstOccupied = KCR_NO_ENTRY;
  } else {
    // Stitch
    getNext(n, manager)->prev = n->prev;
    getPrev(n, manager)->next = n->next;
    // Was the firstOccupied node deleted?
    if (node == manager->firstOccupied) manager->firstOccupied = n->next;
  }
  // Add to free list
  n->size = 1;
  // Can we coälesce to the right?
  bool coalesced = false;
  KCREntryHandle sh = node + 1;
  KCREntry* succ = n + 1;
  if (inBounds(manager, succ) && succ->size != 0) {
    LOGF("coalesce right (size = %zu)\n", succ->size);
    n->size = succ->size + 1;
    getPrev(succ, manager)->next = node;
    getNext(succ, manager)->prev = node;
    n->prev = (succ->prev == sh) ? node : succ->prev;
    n->next = (succ->next == sh) ? node : succ->next;
    // Remember to update the next entry
    KCREntry* nextBlock = succ + succ->size;
    if (inBounds(manager, nextBlock)) { nextBlock->sizePrev = n->size; }
    if (manager->firstFree == sh) manager->firstFree = node;
    coalesced = true;
  }
  // What about to the left?
  if (n->sizePrev != 0) {
    assert(node >= n->sizePrev);
    KCREntry* prevBlock = n - n->sizePrev;
    if (prevBlock->size != 0) {
      LOGF("coalesce left (size = %zu)\n", prevBlock->size);
      // Prev block's size is increased by this block's
      prevBlock->size += n->size;
      // Next block also notes the new sizePrev
      KCREntry* nextBlock = n + n->size;
      if (inBounds(manager, nextBlock)) {
        nextBlock->sizePrev = prevBlock->size;
      }
      n->size = 0;
      // If already coälesced to the right, delete this block from free list
      if (coalesced) {
        getPrev(n, manager)->next = n->next;
        getNext(n, manager)->prev = n->prev;
        if (manager->firstFree == node) manager->firstFree = n->next;
      }
      assert(n->size == 0);
      coalesced = true;
    }
  }
  if (!coalesced) {
    // Add new entry to free list
    if (manager->firstFree == KCR_NO_ENTRY) {
      manager->firstFree = node;
      n->next = node;
      n->prev = node;
    } else {
      n->prev = kcrEntryDeref(manager, manager->firstFree)->prev;
      n->next = manager->firstFree;
      getPrev(n, manager)->next = node;
      getNext(n, manager)->prev = node;
    }
  }
  // Do cleanup; don't need to unwind since the outermost function already
  // returned
  if (!nodeOnly) kcrEntryDestroy(manager, node, false);
  traceRegions(manager);
  kcrCheckConsistency(manager);
}

void kcrManagerFreeEntry(KCRManager* manager, KCREntryHandle node) {
  kcrManagerFreeEntryEx(manager, node, false);
}

KCREntryHandle kcrManagerSpawnD(
    KCRManager* manager, void (*callback)(void*), size_t udSize,
    size_t stackSize, void (*tearDown)(void*), void** udOut) {
  if (stackSize == 0) stackSize = manager->defaultStackSize;
  KCREntryHandle secondEntry =
      manager->firstOccupied != KCR_NO_ENTRY ?
          kcrEntryDeref(manager, manager->firstOccupied)->next :
          KCR_NO_ENTRY;
  KCREntryHandle eh =
      kcrManagerAddEntry(manager, secondEntry, stackSize, udSize);
  if (eh == KCR_NO_ENTRY) return KCR_NO_ENTRY;
  KCREntry* e = kcrEntryDeref(manager, eh);
  e->callback = callback;
  e->tearDown = tearDown;
  e->stackPointer = kcrSetUpStack(e->stackBase, e->callback, e->stackBase);
  *udOut = e->stackBase;
  return eh;
}

KCREntryHandle kcrManagerSpawn(
    KCRManager* manager, void (*callback)(void*), size_t udSize,
    size_t stackSize, void** udOut) {
  return kcrManagerSpawnD(
      manager, callback, udSize, stackSize, kcrTeardownNull, udOut);
}

void kcrManagerExit(KCRManager* manager, KCREntryHandle node) {
  // Prepare to enter next coroutine (if any)
  if (manager->firstOccupied != KCR_NO_ENTRY && manager->advanceOnExit) {
    manager->firstOccupied =
        kcrEntryDeref(manager, manager->firstOccupied)->next;
  }
  KCREntry* n = kcrEntryDeref(manager, node);
  contextSwitch(&(n->stackPointer), manager->outsideStackPointer);
}

bool kcrManagerExitEx(KCRManager* manager, KCREntryHandle node) {
  // Prepare to enter next coroutine (if any)
  if (manager->firstOccupied != KCR_NO_ENTRY && manager->advanceOnExit) {
    manager->firstOccupied =
        kcrEntryDeref(manager, manager->firstOccupied)->next;
  }
  KCREntry* n = kcrEntryDeref(manager, node);
  contextSwitch(&(n->stackPointer), manager->outsideStackPointer);
  // Careful! n is no longer valid, so we need to call kcrEntryDeref again.
  return kcrEntryDeref(manager, node)->destroying;
}

void kcrReturnFromCoroutineMain(void* dummy) {
  (void) dummy;
  KCRManager* m = kcrManagerGetCurrent();
  KCREntryHandle eh = kcrEntryGetCurrent();
  LOGF("kcrReturnFromCoroutineMain of node %zu\n", eh);
  // Called when a coroutine returns.
  // If this coroutine is flagged to be destroyed, then let the destroyer
  // take care of it
  KCREntry* e = kcrEntryDeref(m, eh); // idc, really
  if (e->destroying) { kcrManagerExit(m, eh); }
  // Remove from list
  e = kcrEntryDeref(m, eh); // Reload pointer since it might be invalid
  KCREntryHandle next = e->next;
  kcrManagerFreeEntry(m, eh);
  // If there are no coroutines left, return to outside
  if (m->firstOccupied == KCR_NO_ENTRY) { kcrManagerExit(m, eh); }
  // Switch contexts
  kcrEntrySetCurrent(next);
  m->firstOccupied = next;
  void* sink;
  contextSwitch(&sink, kcrEntryDeref(m, next)->stackPointer);
}

void kcrReturnFromCoroutine(void) {
  // Set up scratch stack
  void* scstack = kcrSetUpStack(&scratchStack, kcrReturnFromCoroutineMain, 0);
  // Switch to a scratch stack before freeing the entry stack in order to avoid
  // stomping on the heap
  KCREntry* ce = kcrEntryDeref(kcrManagerGetCurrent(), kcrEntryGetCurrent());
  contextSwitch(&(ce->stackPointer), scstack);
}

void kcrManagerEnter(KCRManager* manager, KCREntryHandle node) {
  if (node == KCR_NO_ENTRY) node = manager->firstOccupied;
  // Set global vars for use inside coroutines
  KCRManager* oldKCRManager = kcrManagerGetCurrent();
  KCREntryHandle oldKCREntry = kcrEntryGetCurrent();
  kcrManagerSetCurrent(manager);
  kcrEntrySetCurrent(node);
  KCREntry* n = kcrEntryDeref(manager, node);
  // Switch stacks
  contextSwitch(&(manager->outsideStackPointer), n->stackPointer);
  kcrManagerSetCurrent(oldKCRManager);
  kcrEntrySetCurrent(oldKCREntry);
}

void kcrManagerYield(KCRManager* manager, KCREntryHandle node) {
  // Switch to next coroutine in manager while not outright
  // ending the current one
  KCREntry* n = kcrEntryDeref(manager, node);
  if (n->destroying) { kcrManagerExit(manager, node); }
  if (node == n->next) return;
  manager->firstOccupied = n->next;
  kcrEntrySetCurrent(n->next);
  contextSwitch(&(n->stackPointer), getNext(n, manager)->stackPointer);
  // Reload n since it might have been invalidated
  n = kcrEntryDeref(manager, node);
  if (n->destroying) { kcrManagerExit(manager, node); }
}

bool kcrManagerYieldEx(KCRManager* manager, KCREntryHandle node) {
  // Switch to next coroutine in manager while not outright
  // ending the current one
  KCREntry* n = kcrEntryDeref(manager, node);
  if (node == n->next) return false;
  manager->firstOccupied = n->next;
  kcrEntrySetCurrent(n->next);
  contextSwitch(&(n->stackPointer), getNext(n, manager)->stackPointer);
  n = kcrEntryDeref(manager, node);
  return n->destroying;
}

bool kcrManagerAdvancesOnExit(const KCRManager* manager) {
  return manager->advanceOnExit;
}

void kcrManagerSetAdvanceOnExit(KCRManager* manager, bool value) {
  manager->advanceOnExit = value;
}

size_t kcrManagerDefaultStackSize(const KCRManager* manager) {
  return manager->defaultStackSize;
}

void kcrManagerSetDefaultStackSize(KCRManager* manager, size_t value) {
  manager->defaultStackSize = value;
}

KCREntryHandle kcrManagerGetFirstOccupied(KCRManager* manager) {
  return manager->firstOccupied;
}

void kcrEnter(void) {
  kcrManagerEnter(kcrManagerGetCurrent(), kcrEntryGetCurrent());
}

void kcrExit(void) {
  kcrManagerExit(kcrManagerGetCurrent(), kcrEntryGetCurrent());
}

bool kcrExitEx(void) {
  return kcrManagerExitEx(kcrManagerGetCurrent(), kcrEntryGetCurrent());
}

void kcrYield(void) {
  kcrManagerYield(kcrManagerGetCurrent(), kcrEntryGetCurrent());
}

bool kcrYieldEx(void) {
  return kcrManagerYieldEx(kcrManagerGetCurrent(), kcrEntryGetCurrent());
}

void kcrSpawn(
    void (*callback)(void*), size_t udSize, size_t stackSize, void** udOut) {
  kcrManagerSpawn(kcrManagerGetCurrent(), callback, udSize, stackSize, udOut);
}

void kcrSpawnD(
    void (*callback)(void*), size_t udSize, size_t stackSize,
    void (*tearDown)(void*), void** udOut) {
  kcrManagerSpawnD(
      kcrManagerGetCurrent(), callback, udSize, stackSize, tearDown, udOut);
}
