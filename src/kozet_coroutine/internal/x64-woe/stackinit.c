/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdbool.h>
#include <stdint.h>

extern void kcrReturnFromCoroutine(void);

extern void returnFromCoroutineThunk(void);

void* kcrSetUpStack(void* base, void* returnAddress, void* userdata) {
  void** baseOfPointers = (void**) base;
  baseOfPointers[-2] = (void*) &returnFromCoroutineThunk;
  baseOfPointers[-3] = (void*) &returnFromCoroutineThunk;
  baseOfPointers[-4] = returnAddress; // return address
  baseOfPointers[-5] = userdata; // Here, userdata should be stored in rcx
  // 2 return addresses + 10 registers saved to stack + 2 words of pad
  return baseOfPointers - 13;
}
