/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#pragma once
#ifndef KOZET_COROUTINE_H
#  define KOZET_COROUTINE_H

#  include <stddef.h>

#  include <stdbool.h>

#  ifdef __cplusplus
extern "C" {
#  endif

/** \mainpage kozet_coroutine
 *
 * \section intro_sec Introduction & Rationale
 *
 * kozet_coroutine, or kcr for short, is a library that allows creating tasks
 * that can be suspended (yielded).
 *
 * Include the header `<kozet_coroutine/kcr.h>` and link to
 * `libkozet_coroutine.a` to get started. If this header is included in a C++
 * source, and `KCR_NO_CPP_WRAPPER` is not defined, then a C++ wrapper is
 * also provided.
 *
 * \section changelog Changelog
 *
 * \subsection v02 0.2
 *
 * - `KCREntry*` has been replaced with `KCREntryHandle` in order to avoid
 *   nasty pointer invalidation bugs. Instead of null, use \ref KCR_NO_ENTRY.
 * - C++ wrapper should be unaffected.
 */

/**
 * \brief A manager for tasks.
 *
 * Note that this is an opaque type, so only pointers to this type can be used.
 */
typedef struct KCRManager KCRManager;

// TODO: KCREntryHandle-based API for C++ wrapper
// TODO: use a struct instead?
/**
 * \brief A type representing a task.
 *
 * Unlike a pointer to \ref KCREntry, this type is not invalidated when
 * spawning a new task causes the internal queue to be resized.
 */
typedef size_t KCREntryHandle;

/// A value that indicates an absence of an entry.
#  define KCR_NO_ENTRY (-1)

/// Get the default stack size for new `KCRManager`s.
size_t kcrDefaultStackSize(void);

/**
 * \brief Create a new task manager.
 *
 * \return A pointer to a `KCRManager`, or null if failed.
 */
KCRManager* kcrManagerCreate(void);
/**
 * \brief Destroy a task manager.
 *
 * \param manager The manager to destroy.
 */
void kcrManagerDestroy(KCRManager* manager);
/**
 * \brief Spawn a new task in the manager.
 *
 * \param manager The manager in which to create the entry.
 * \param callback The function to run in this task.
 * \param udSize The size of the user data. A pointer to this area of memory
 *   will be passed to the callback.
 * \param stackSize The size of the stack, or 0 for the default.
 * \param udOut Pointer to where you want the user data to be stored.
 * \return An entry handle to the new task.
 */
KCREntryHandle kcrManagerSpawn(
    KCRManager* manager, void (*callback)(void*), size_t udSize,
    size_t stackSize, void** udOut);
/**
 * \brief Spawn a new task in the manager.
 *
 * \param manager The manager in which to create the entry.
 * \param callback The function to run in this task.
 * \param udSize The size of the user data. A pointer to this area of memory
 *   will be passed to the callback and teardown functions.
 * \param stackSize The size of the stack, or 0 for the default.
 * \param tearDown The function to run when the task is finished
 * \param udOut Pointer to where you want the user data to be stored.
 * \return An entry handle to the new task.
 */
KCREntryHandle kcrManagerSpawnD(
    KCRManager* manager, void (*callback)(void*), size_t udSize,
    size_t stackSize, void (*tearDown)(void*), void** udOut);

/**
 * \brief Remove a task from a manager.
 *
 * Don't call this function to delete a task that you are currently in.
 *
 * \param manager The manager from which to remove the task.
 * \param node An entry to the task to remove.
 */
void kcrManagerFreeEntry(KCRManager* manager, KCREntryHandle node);
/**
 * \brief Enter a task in a manager.
 *
 * \param manager The manager that owns the task to enter.
 * \param node The task to enter. If this is \ref KCR_NO_ENTRY, then the "first
 * occupied" node will be entered.
 */
void kcrManagerEnter(KCRManager* manager, KCREntryHandle node);
/**
 * \brief Exit from a task.
 *
 * It is usually a better idea to use `kcrExit`.
 *
 * \param manager The manager that owns the task to exit.
 * \param node The task to exit.
 */
void kcrManagerExit(KCRManager* manager, KCREntryHandle node);
/**
 * \brief Exit from a task.
 *
 * It is usually a better idea to use \ref kcrExitEx.
 *
 * \param manager The manager that owns the task to exit.
 * \param node The task to exit.
 * \return true if the task is about to be destroyed.
 */
bool kcrManagerExitEx(KCRManager* manager, KCREntryHandle node);
/**
 * \brief Yield from a task.
 *
 * It is usually a better idea to use \ref kcrYield.
 *
 * Note that this function does not unwind the stack; if you have resources
 * owned inside the callback body of the task, then it is better to use
 * \ref kcrManagerYieldEx or \ref kcrYieldEx, and clean up and return
 * early if that returns true.
 *
 * \param manager The manager that owns the task to yield from.
 * \param node The task to yield from.
 */
void kcrManagerYield(KCRManager* manager, KCREntryHandle node);
/**
 * \brief Yield from a task.
 *
 * It is usually a better idea to use \ref kcrYieldEx.
 *
 * \param manager The manager that owns the task to yield from.
 * \param node The task to yield from.
 * \return true if the task is about to be destroyed.
 */
bool kcrManagerYieldEx(KCRManager* manager, KCREntryHandle node);
/**
 * \brief Return whether the manager will advance a task when exiting.
 *
 * That is, when \ref kcrManagerExit or \ref kcrManagerExitEx is called, then
 * the next entry will land in the task following the one that exited if doing
 * so is enabled. If the manager is set to not advance on exit, the the next
 * entry will land in the task that exited.
 */
bool kcrManagerAdvancesOnExit(const KCRManager* manager);
/**
 * \brief Set whether the manager will advance a task when exiting.
 *
 * See \ref kcrManagerAdvancesOnExit for more details.
 */
void kcrManagerSetAdvanceOnExit(KCRManager* manager, bool value);
/**
 * \brief Get the default stack size that will be used for tasks spawned from
 * this manager.
 */
size_t kcrManagerDefaultStackSize(const KCRManager* manager);
/**
 * \brief Set the default stack size that will be used for tasks spawned from
 * this manager.
 */
void kcrManagerSetDefaultStackSize(KCRManager* manager, size_t value);
/**
 * \brief Return the "first occupied" task of the manager.
 *
 * This is the task that the manager will enter next by default when
 * \ref kcrManagerEnter is called.
 *
 * \return The task, or \ref KCR_NO_ENTRY if there are none.
 */
KCREntryHandle kcrManagerGetFirstOccupied(KCRManager* manager);

/**
 * \brief Similar to \ref kcrManagerEnter, but passes the current manager and
 * entry.
 */
void kcrEnter(void);
/**
 * \brief Similar to \ref kcrManagerExit, but passes the current manager and
 * entry.
 */
void kcrExit(void);
/**
 * \brief Similar to \ref kcrManagerExitEx, but passes the current manager and
 * entry.
 */
bool kcrExitEx(void);
/**
 * \brief Similar to \ref kcrManagerYield, but passes the current manager and
 * entry.
 */
void kcrYield(void);
/**
 * \brief Similar to \ref kcrManagerYieldEx, but passes the current manager and
 * entry.
 */
bool kcrYieldEx(void);
/**
 * \brief Similar to \ref kcrSpawn, but passes the current manager.
 */
void kcrSpawn(
    void (*callback)(void*), size_t udSize, size_t stackSize, void** udOut);
/**
 * \brief Similar to \ref kcrSpawnD, but passes the current manager.
 */
void kcrSpawnD(
    void (*callback)(void*), size_t udSize, size_t stackSize,
    void (*tearDown)(void*), void** udOut);

/// Get the current manager.
KCRManager* kcrManagerGetCurrent(void);
/// Get the current task.
KCREntryHandle kcrEntryGetCurrent(void);

#  ifdef __cplusplus
}
// C++ wrapper code will go here
#    ifndef KCR_NO_CPP_WRAPPER

#      include <iostream>
#      include <tuple>
#      include <utility>

extern "C" void kcrTeardownNull(void* userdata);

namespace kcr {
#      ifndef KCR_NO_CPP_UNWIND
  /// An exception type to signal an unwind.
  class Unwind {};
#      endif
#      if __cplusplus < 201703L
  template<typename F, typename T, size_t... I>
  auto apply2(F&& f, T&& t, std::index_sequence<I...> seq) {
    (void) seq;
    return f(std::get<I>(t)...);
  }
  template<typename F, typename T> auto apply(F&& f, T&& t) {
    return apply2(f, t, std::make_index_sequence<std::tuple_size<T>::value>());
  }
#      endif
  template<typename UD> void proxyCallback(void* userdata) {
#      ifndef KCR_NO_CPP_UNWIND
    try {
#      endif
      UD* data = (UD*) userdata;
      apply(std::get<0>(*data), std::get<2>(*data));
#      ifndef KCR_NO_CPP_UNWIND
    } catch (Unwind&) { /* NOTHING!!! */
    }
#      endif
  }
  template<typename UD> void proxyDestroy(void* userdata) {
    UD* data = (UD*) userdata;
    apply(std::get<1>(*data), std::get<2>(*data));
    data->~UD();
  }
  /**
   * \brief A non-owning handle to a manager.
   */
  class ManagerObserver {
  public:
    /**
     * \brief A non-owning handle to an entry.
     */
    class Entry {
    public:
      Entry(KCREntryHandle e) : ent(e) {}
      Entry() : ent(KCR_NO_ENTRY) {}
      /// Get the raw `KCREntry*` value.
      KCREntryHandle underlying() { return ent; }
      Entry(const Entry& e) = default;
      Entry(Entry&& e) = default;

    private:
      KCREntryHandle ent;
    };
    ManagerObserver(KCRManager* m) : man(m) {}
    ManagerObserver(const ManagerObserver& m) = default;
    ManagerObserver(ManagerObserver&& m) = default;
    ManagerObserver& operator=(const ManagerObserver& m) = default;
    ManagerObserver& operator=(ManagerObserver&& m) = default;
    /// A thin wrapper around \ref kcrManagerSpawnD.
    Entry spawnRaw(
        void*& udOut, void (*callback)(void*), size_t udSize,
        size_t stackSize = 0, void (*tearDown)(void*) = kcrTeardownNull) {
      KCREntryHandle e =
          kcrManagerSpawnD(man, callback, udSize, stackSize, tearDown, &udOut);
      return Entry(e);
    }
    /**
     * \brief Spawn a task with the default stack size.
     *
     * \param callback A callable object to run in this task.
     * \param tearDown A callable object to call when this task is finished.
     * \param args Zero or more arguments to pass to `callback` and `tearDown`.
     * \return An entry handle.
     */
    template<typename F1, typename F2, typename... Args>
    Entry spawnTeardown(F1&& callback, F2&& tearDown, Args&&... args) {
      using T = std::tuple<
          std::decay_t<F1>, std::decay_t<F2>,
          std::tuple<std::decay_t<Args>...>>;
      void* ud;
      Entry e = spawnRaw(ud, proxyCallback<T>, sizeof(T), 0, proxyDestroy<T>);
      T* udt = (T*) ud;
      new (udt)
          T(std::forward<F1>(callback), std::forward<F2>(tearDown),
            std::tuple<std::decay_t<Args>...>(std::forward<Args>(args)...));
      return e;
    }
    /**
     * \brief Spawn a task with the default stack size.
     *
     * This is identical to \ref spawnTeardown, but with no teardown handler.
     *
     * \param callback A callable object to run in this task.
     * \param args Zero or more arguments to pass to `callback` and `tearDown`.
     * \return An entry handle.
     */
    template<typename F1, typename... Args>
    Entry spawn(F1&& callback, Args&&... args) {
      return spawnTeardown(
          callback, [](const Args&... /*args*/) {},
          std::forward<Args>(args)...);
    }
    /**
     * \brief Enter the manager.
     */
    void enter() { kcrManagerEnter(man, KCR_NO_ENTRY); }
    /**
     * \brief Enter the manager at a given task.
     */
    void enter(Entry e) { kcrManagerEnter(man, e.underlying()); }
    /**
     * \brief Suspend control of the task and transfer to the next one.
     *
     * This method automatically handles unwinding, unless you have
     * `KCR_NO_CPP_UNWIND` defined, and you should think twice before doing
     * that.
     *
     * That being said, it's better to use \ref kcr::yield().
     */
    void yield(Entry e) {
#      ifndef KCR_NO_CPP_UNWIND
      if (kcrManagerYieldEx(man, e.underlying())) throw Unwind{};
#      else
      kcrManagerYield(man, e.underlying());
#      endif
    }
    /**
     * \brief Suspend control of the task and transfer to the next one.
     *
     * Unlike with \ref yield, unwinding is not done automatically. You will
     * need to check the return value as with \ref kcrManagerYieldEx.
     *
     * It's probably better to use \ref kcr::yieldNoThrow().
     *
     * \return true if the task is about to be destroyed
     */
    bool yieldNoThrow(Entry e) noexcept {
      return kcrManagerYieldEx(man, e.underlying());
    }
    /**
     * \brief Exit from a task.
     *
     * This method automatically handles unwinding, unless you have
     * `KCR_NO_CPP_UNWIND` defined, and you should think twice before doing
     * that.
     *
     * It's probably better to use \ref kcr::exit().
     */
    void exit(Entry e) {
#      ifndef KCR_NO_CPP_UNWIND
      if (kcrManagerExitEx(man, e.underlying())) throw Unwind{};
#      else
      kcrManagerExit(man, e.underlying());
#      endif
    }
    /**
     * \brief Exit from a task.
     *
     * Unlike with \ref exit, unwinding is not done automatically. You will
     * need to check the return value as with \ref kcrManagerExitEx.
     *
     * It's probably better to use \ref kcr::exitNoThrow().
     *
     * \return true if the task is about to be destroyed
     */
    bool exitNoThrow(Entry e) noexcept {
      return kcrManagerExitEx(man, e.underlying());
    }
    /// Set whether the manager advances on exit.
    void setAdvanceOnExit(bool e) { kcrManagerSetAdvanceOnExit(man, e); }
    /// Get whether the manager advances on exit.
    bool advancesOnExit() const { return kcrManagerAdvancesOnExit(man); }
    /// Get the raw `KCRManager*` value.
    KCRManager* underlying() { return man; }

  private:
    KCRManager* man;
    friend class Manager;
  };
  /**
   * \brief An owning handle to a manager.
   *
   * This class is not copy-constructible or -assignable. Otherwise, it has
   * the same methods as \ref ManagerObserver.
   */
  class Manager : public ManagerObserver {
  public:
    Manager() : ManagerObserver(kcrManagerCreate()) {}
    Manager(KCRManager* m) noexcept : ManagerObserver(m) {}
    ~Manager() {
      if (man != nullptr) kcrManagerDestroy(man);
    }
    Manager(const Manager& m) = delete;
    Manager(Manager&& m) noexcept : ManagerObserver(m.man) { m.man = nullptr; }
    Manager& operator=(const Manager& m) = delete;
    Manager& operator=(Manager&& m) noexcept {
      std::swap(man, m.man);
      return *this;
    }
  };
  static_assert(
      std::is_copy_constructible<ManagerObserver>::value &&
      std::is_copy_assignable<ManagerObserver>::value);
  static_assert(
      std::is_move_constructible<Manager>::value &&
      std::is_move_assignable<Manager>::value);
  /**
   * \brief Similar to \ref ManagerObserver::enter, but passes the current
   * manager and entry.
   */
  inline void enter() { kcrEnter(); }
  /**
   * \brief Similar to \ref ManagerObserver::exit, but passes the current
   * manager and entry.
   */
  inline void exit() {
#      ifndef KCR_NO_CPP_UNWIND
    if (kcrExitEx()) throw Unwind{};
#      else
    kcrExit();
#      endif
  }
  /**
   * \brief Similar to \ref ManagerObserver::yield, but passes the current
   * manager and entry.
   */
  inline void yield() {
#      ifndef KCR_NO_CPP_UNWIND
    if (kcrYieldEx()) throw Unwind{};
#      else
    kcrYield();
#      endif
  }
  /**
   * \brief Similar to \ref ManagerObserver::yieldNoThrow, but passes the
   * current manager and entry.
   */
  inline bool yieldNoThrow() noexcept { return kcrYieldEx(); }
  /// Get the current manager.
  inline ManagerObserver getCurrentManager() {
    return ManagerObserver(kcrManagerGetCurrent());
  }
  /// Get the current task.
  inline ManagerObserver::Entry getCurrentEntry() {
    return ManagerObserver::Entry(kcrEntryGetCurrent());
  }
  /**
   * \brief Similar to \ref ManagerObserver::spawnRaw, but passes the
   * current manager.
   */
  inline void spawnRaw(
      void*& udOut, void (*callback)(void*), size_t udSize,
      size_t stackSize = 0, void (*tearDown)(void*) = kcrTeardownNull) {
    kcrSpawnD(callback, udSize, stackSize, tearDown, &udOut);
  }
  /**
   * \brief Similar to \ref ManagerObserver::spawnTeardown, but passes the
   * current manager.
   */
  template<typename F1, typename F2, typename... Args>
  inline void spawnTeardown(F1&& callback, F2&& tearDown, Args&&... args) {
    getCurrentManager().spawnTeardown(
        callback, tearDown, std::forward<Args>(args)...);
  }
  /**
   * \brief Similar to \ref ManagerObserver::spawn, but passes the
   * current manager.
   */
  template<typename F1, typename... Args>
  inline void spawn(F1&& callback, Args&&... args) {
    getCurrentManager().spawn(callback, std::forward<Args>(args)...);
  }
}

#    endif
#  endif

#endif // KOZET_COROUTINE_H
