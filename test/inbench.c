#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <kozet_coroutine/kcr.h>
#include <stdbool.h>

void spinner(void* x) {
  size_t n = *(size_t*) x;
  for (size_t i = 0; i < n; ++i) { kcrYield(); }
}

void mediator(void* x) {
  (void) x;
  while (true) {
    kcrYield();
    kcrExit();
  }
}

/*
Takes an input file in the following format:
until the file ends:
- <1 byte> # of times to loop
- <1 byte> userdata size in multiples of 8 and with 8 added
The manager is entered, going through each task once, after each entry.
*/

int main(int argc, char** argv) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <file>\n", argv[0]);
    return -1;
  }
  FILE* fh = fopen(argv[1], "rb");
  if (fh == 0) {
    perror("Couldn't open file");
    return -1;
  }
  fseek(fh, 0, SEEK_END);
  size_t fSize = ftell(fh);
  fseek(fh, 0, SEEK_SET);
  if (fSize % 2 != 0) {
    fprintf(stderr, "Odd number of bytes in file\n");
    fclose(fh);
    return -1;
  }
  unsigned char* input = malloc(fSize * sizeof(*input));
  if (input == 0) {
    fprintf(stderr, "Allocation failed\n");
    fclose(fh);
    return -1;
  }
  size_t read = fread(input, fSize, 1, fh);
  if (read == 0) {
    fprintf(stderr, "Failed to read from file\n");
    fclose(fh);
    free(input);
    return -1;
  }
  size_t nElems = fSize / 2;
  KCRManager* man = kcrManagerCreate();
  if (man == 0) {
    fprintf(stderr, "Creating manager failed\n");
    fclose(fh);
    free(input);
    return -1;
  }
  void* ud;
  KCREntryHandle theMediator = kcrManagerSpawn(man, mediator, 0, 0, &ud);
  clock_t cl = clock();
  for (size_t i = 0; i < nElems; ++i) {
    size_t iterations = input[2 * i];
    size_t udSize = 8 * input[2 * i + 1] + 8;
    kcrManagerSpawn(man, spinner, udSize, 0, &ud);
    *(size_t*) ud = iterations;
    kcrManagerEnter(man, theMediator);
  }
  clock_t cl2 = clock();
  printf("%f ns\n", (cl2 - cl) * 1e9 / CLOCKS_PER_SEC);
  kcrManagerDestroy(man);
  free(input);
  fclose(fh);
  return 0;
}
